﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using AngularFun.Models;
using System.Web;

namespace AngularFun.Controllers
{
    public class CarController : ApiController
    {
        // GET: api/Car
        public IEnumerable<Car> Get()
        {
            HttpContext.Current.Session["thing"] = 123;

            return new List<Car>
            {
                new Car("Ferrari", "http://488gtb.ferrari.com/img/carHome.png", 1000),
                new Car("Van", "http://www.vanhiresowerbybridge.co.uk/images/van.jpg", 234)
            };
        }

        // GET: api/Car/5
        public string Get(int id)
        {
            return "value";
        }

        // POST: api/Car
        public void Post([FromBody]Car value)
        {
            // write a new car into the DB
        }

        // PUT: api/Car/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/Car/5
        public void Delete(int id)
        {
        }
    }
}
