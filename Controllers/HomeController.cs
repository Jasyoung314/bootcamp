﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using AngularFun.Models;

namespace AngularFun.Controllers
{
    public class HomeController : Controller
    {
        public ActionResult Index()
        {
            Session["thing"] = 123;
            this.Session["thong"] = 456;
            base.Session["bong"] = 123;
            return View();
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Showroom()
        {
            // this is in place of a DB dip
            Car featuredCar = new Car(
                "Skoda",
                "https://upload.wikimedia.org/wikipedia/commons/7/77/Skoda_105S_-_front.jpg",
                10);

            return View(featuredCar);
        }
    }
}